FROM node:8-alpine AS build

# Set up working directory
ENV APP_HOME=/marketplace
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
ADD . $APP_HOME

# Install node module packages
RUN yarn install
RUN yarn build

FROM nginx:stable
COPY --from=build $APP_HOME/marketplace/dist $APP_HOME/dist
RUN rm /etc/nginx/conf.d/default.conf
RUN cp /etc/nginx/nginx.conf /etc/nginx/nginx_o.conf
COPY --from=build $APP_HOME/marketplace/nginx.conf $APP_HOME/etc/nginx/nginx.conf
RUN mv $APP_HOME/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
