import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Pagination from '../Pagination';

class Table extends React.Component {
  static getDerivedStateFromProps(props, state) {
    if (props.data !== state.data || props.page !== state.page || props.totalCount !== state.totalCount) {
      return {
        data: props.data,
        page: props.page,
        totalCount: props.totalCount
      };
    }
    return null;
  }

  constructor(props) {
    super(props);
    this.state = {
      keyField: this.props.keyField,
      data: this.props.data,
      columns: _.sortBy(this.props.columns, 'colPosition'),
      className: this.props.className,
      toggleSort: false,
      dataKey: '',
      count: this.props.count,
      totalCount: this.props.totalCount,
      pagination: this.props.pagination,
      page: this.props.page
    };
  }

  onSort = (event, colItem) => {
    const { handleSort } = this.props;
    const { dataKey } = this.state;
    const { toggleSort } = this.state;
    if (dataKey === colItem.dataField) {
      this.setState({ toggleSort: !toggleSort });
    } else {
      this.setState({ toggleSort: true });
    }
    this.setState({ dataKey: colItem.dataField });
    return (handleSort(colItem.dataField, colItem.isSortable, toggleSort ? 'ASC' : 'DESC'));
  }

  render() {
    const {
      className, columns, data, keyField, toggleSort, dataKey, count, totalCount, pagination, page
    } = this.state;
    const { setpagination } = this.props;
    return (
      <Fragment>
        <div className="table-responsive">
          <table className={className !== '' ? `table ${className}` : 'table'}>
            <thead>
              <tr>
                {columns.map(colItem => (
                  <td key={colItem.colPosition} className={`w-${colItem.width}`} >
                    <span
                      role="presentation"
                      onClick={event => this.onSort(event, colItem)}
                    >
                      {colItem.text}
                      {colItem.isSortable &&
                        <i
                          className={`fa ml-1 fa-sort-${dataKey === colItem.dataField && toggleSort ?
                            'down' : 'up'}`}
                        />
                      }
                    </span>
                  </td>))}
              </tr>
            </thead>
            <tbody>
              {
                data && data.map(item => (
                  <tr key={item[keyField]}>
                    {
                      columns.map(colItem => (
                        <td
                          key={colItem.colPosition}
                        >
                          {colItem.customColumn ? colItem.customColumn(item, this.props)
                            : item[colItem.dataField] || '-' }
                        </td>
                      ))
                    }
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
        {pagination &&
          <Pagination
            count={count}
            totalCount={totalCount}
            page={page}
            setpagination={pageNo => setpagination(pageNo)}
          />
        }
      </Fragment>
    );
  }
}

Table.propTypes = {
  keyField: PropTypes.string.isRequired,
  setpagination: PropTypes.func,
  data: PropTypes.array,
  columns: PropTypes.array.isRequired,
  className: PropTypes.string,
  handleSort: PropTypes.func.isRequired,
  count: PropTypes.number,
  totalCount: PropTypes.number,
  pagination: PropTypes.bool,
  page: PropTypes.number

};

Table.defaultProps = {
  data: [],
  className: '',
  count: 0,
  totalCount: 0,
  pagination: false,
  setpagination: () => {},
  page: 1
};

export default Table;
