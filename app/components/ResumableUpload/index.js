import React, { Fragment, Component } from 'react';
import tus from 'tus-js-client';

import BaseURL from '../../utils/BaseUrl';
import '../../../assets/styles/components/ResumableUpload.scss';

class ResumableUpload extends Component {
  constructor() {
    super();
    this.state = {
      uploads: [],
      isUploadingStarted: false,
      isUploadingPaused: false,
      totalFileSize: 0,
      uploadCompletedFileSize: 0
    };
  }

  onDropFile = event => {
    Array.from(event.target.files).map(file => {
      if (file) {
        const upload = new tus.Upload(file, {
          endpoint: `${BaseURL()}/files`,
          // endpoint: 'https://master.tus.io/files/',
          retryDelays: [0, 1000, 3000, 5000],
          metadata: {
            filename: file.name,
            filetype: file.type
          },
          onError: error => {
            console.log('error : --', error);
          },
          onProgress: (bytesUploaded, bytesTotal) => {
            this.setState({
              [file.name]: bytesUploaded
            });
          },
          onSuccess: () => {
            console.log('Success : --', upload.file.name, upload.url);
          }
        });
        this.setState(previousState => ({
          uploads: [...previousState.uploads, upload],
          totalFileSize: previousState.totalFileSize + file.size
        }));
      }
    });
  }

  onClickPauseOrResume = () => {
    const { isUploadingPaused } = this.state;
    return (
      !isUploadingPaused ? this.pauseUpload : this.resumeUpload
    );
  }

  getButtonText = () => {
    const { isUploadingPaused, isUploadingStarted } = this.state;
    if (!isUploadingStarted) {
      return 'Start';
    }
    return (!isUploadingPaused ? 'Pause' : 'Resume');
  }

  startUpload = () => {
    const { uploads } = this.state;
    if (uploads.length > 0) {
      uploads.map(upload => (
        upload.start()
      ));
      this.setState({
        isUploadingStarted: true
      });
    }
  }

  pauseUpload = () => {
    const { uploads } = this.state;
    if (uploads.length > 0) {
      uploads.map(upload => (
        upload.abort()
      ));
      this.setState({
        isUploadingPaused: true
      });
    }
  }

  resumeUpload = () => {
    const { uploads } = this.state;
    if (uploads.length > 0) {
      uploads.map(upload => (
        upload.start()
      ));
      this.setState({
        isUploadingPaused: false
      });
    }
  }

  deleteFile = fileSize => {
    const newUploads = this.state.uploads.filter(upload => (
      fileSize !== upload.file.size
    ));
    this.setState(state => ({
      uploads: newUploads,
      isUploadingStarted: false,
      isUploadingPaused: false,
      totalFileSize: state.totalFileSize - fileSize
    }));
  }

  render() {
    const {
      isUploadingStarted,
      uploads,
      totalFileSize
    } = this.state;
    let completedFileSize = 0;
    uploads.map(upload => {
      completedFileSize = parseInt((completedFileSize + this.state[upload.file.name]), 10);
      return completedFileSize;
    });
    const completedPercentage = ((completedFileSize / totalFileSize) * 100).toFixed(2);
    return (
      <div className="container-fluid resumableUpload-container">
        <div className="row justify-content-center">
          <div className="col-12">
            <section className="dropFiles fileUpload p-4">
              {uploads.length === 0 &&
                <div className="content" >
                  <span className="addIcon">+</span>
                  <h5 className="text-dark">Drag &amp; Drop </h5>
                  <h5 className="text-dark"> or</h5>
                  <h5 className="text-dark"> Select your files to start uploading!</h5>
                  <span className="text-primary">...up to 3GB</span>
                  <input
                    type="file"
                    className="uploadFiles"
                    onChange={this.onDropFile}
                    multiple
                  />
                </div>
              }
              {uploads.length > 0 &&
                <Fragment>
                  {uploads.map(upload => (
                    <div className="fileDetails d-flex justify-content-between mb-4" key={upload.file.lastModified}>
                      <div>
                        <div className="fileNameContainer">
                          <span title={upload.file.name}> {upload.file.name}</span>
                        </div>
                      </div>
                      <span
                        role="presentation"
                        className="pointer d-flex align-items-center delIcon"
                        onClick={() => this.deleteFile(upload.file.size)}
                      >
                        x
                      </span>
                    </div>
                  ))}
                  {isUploadingStarted &&
                    <div className="progress">
                      <div
                        className="progress-bar progress-bar-striped"
                        style={{ width: `${completedPercentage}%` }}
                        role="progressbar"
                        aria-valuenow={completedPercentage}
                        aria-valuemin="0"
                        aria-valuemax="100"
                      />
                    </div>
                  }
                  <div className="action-buttons py-3">
                    <div className="text-right">
                      <div className="fileAdd d-flex align-items-center">
                        <div className="cubeBg mr-2">
                          <span>+</span>
                        </div>
                        <div className="d-flex flex-column">
                          <span>Add your files</span>
                          <span className="subFont">
                            {uploads.length} file added - 30 GB Remaining
                          </span>
                        </div>
                        <input type="file" className="uploadFile1" onChange={this.onDropFile} multiple />
                      </div>
                      <button
                        className="btn action-btn"
                        onClick={!isUploadingStarted ? this.startUpload : this.onClickPauseOrResume()}
                        disabled={parseInt(completedPercentage, 10) >= 100}
                      >
                        {this.getButtonText()} upload
                      </button>
                    </div>
                  </div>
                </Fragment>
              }
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default ResumableUpload;
