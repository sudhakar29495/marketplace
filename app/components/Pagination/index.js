import React from 'react';
import PropTypes from 'prop-types';
import '../../../assets/styles/components/Pagination.scss';

class Pagination extends React.Component {
  handleBackward = () => {
    this.props.setpagination(1);
  }
  handleBack = () => {
    const { page } = this.props;
    this.props.setpagination(page === 1 ? page : page - 1);
  }
  handleFront = () => {
    const { page, count, totalCount } = this.props;
    if ((Math.ceil(totalCount / count)) > page) {
      this.props.setpagination(page + 1);
    }
  }
  handleForward= () => {
    const { count, totalCount } = this.props;
    if (Math.ceil(totalCount / count) !== 0) {
      this.props.setpagination(Math.ceil(totalCount / count));
    }
  }
  handlePageClick= e => {
    this.props.setpagination(parseInt(e.target.innerText, 10));
  }

  render() {
    const { page, count, totalCount } = this.props;
    return (
      <section className="d-flex justify-content-center my-3">
        <div className="paginationContainer d-flex">
          <span role="presentation" className="fa fa-angle-double-left" onClick={this.handleBackward} />
          <span role="presentation" className="fa fa-angle-left" onClick={this.handleBack} />
          {count < totalCount && page === 1 &&
            <div>
              <span >{' '}</span>
              <span role="presentation" onClick={this.handlePageClick} className="active">{page}</span>
              <span role="presentation" onClick={this.handlePageClick}>{page + 1}</span>
            </div>
          }
          {count < totalCount && (Math.ceil(totalCount / count)) === page &&
            <div>
              <span role="presentation" onClick={this.handlePageClick}>{page - 1}</span>
              <span role="presentation" onClick={this.handlePageClick} className="active">{page}</span>
              <span>{' '}</span>
            </div>
          }
          {count >= totalCount && page === 1 &&
            <div>
              <span >{' '}</span>
              <span role="presentation" onClick={this.handlePageClick} className="active">{page}</span>
              <span>{' '}</span>
            </div>
          }
          {(page !== 1 && (Math.ceil(totalCount / count)) !== page) &&
          <div>
            <span role="presentation" onClick={this.handlePageClick}>{page - 1}</span>
            <span role="presentation" onClick={this.handlePageClick} className="active">{page}</span>
            <span role="presentation" onClick={this.handlePageClick}>{page + 1}</span>
          </div>
          }
          <span role="presentation" className="fa fa-angle-right" onClick={this.handleFront} />
          <span role="presentation" className="fa fa-angle-double-right" onClick={this.handleForward} />
        </div>
      </section>
    );
  }
}

Pagination.propTypes = {
  setpagination: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
  totalCount: PropTypes.number.isRequired
};

export default Pagination;
