import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import UserAction from '../../actions/user';

import '../../../assets/styles/components/AppHeader.scss';
// import i18n from '../../i18next';

// const AppHeader = props => (
class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // defaultOption: 'Transfer',
      // userId: store.getItem('user')

    };
  }

  static getDerivedStateFromProps(props) {
    return {
      defaultOption: props.defaultChoice
    };
  }

  handleLogout = () => {
    this.props.onlogout();
  }

  render() {
    return (
      <React.Fragment>
        <header className="deepHeader d-flex justify-content-between pl-2">
          <div className="col-4">
            {/* <img
              src="https://cdn.deeptransfer.net/assets/logo-crest.svg"
              className="logo"
              alt="logo"
            />
            <img
              src="https://cdn.deeptransfer.net/assets/logo-tekst.svg"
              className="logoText"
              alt="DEEPTRANSFER"
            /> */}
          </div>
          {/* <div className="d-flex justify-content-center col-4">
            <label
              className={`labelName col-6 text-center ${this.state.defaultOption === 'Transfer' &&
            'activeOption'}`}
              role="presentation"
              onClick={() => this.handleChangeDefaultValue('Transfer')}
              htmlFor="options"
            > Transfer
            </label>
            <label
              className={`labelName col-6 text-center ${this.state.defaultOption === 'MyLinks' &&
            'activeOption'}`}
              htmlFor="options"
              role="presentation"
              onClick={() => this.handleChangeDefaultValue('MyLinks')}
            > My Links
            </label>

          </div> */}
          <div className="d-flex justify-content-center col-4">
            <ul className="d-flex m-0">
              <li className="menu-items">
                <NavLink
                  exact
                  to="/transfer"
                  className="labelName col-6 text-center "
                  activeclassname="active"
                >
                  <i className="navIcon dashboard" />
                  {/* <span>{i18n.t('LABEL.TRANSFER')}</span> */}
                  <span>MarketPlace</span>
                </NavLink>
              </li>
              <li className="menu-items">
                <NavLink
                  exact
                  to="/myLinks"
                  className="labelName col-6 text-center "

                  activeclassname="active"
                >
                  <i className="navIcon dashboard" />
                  <span>{i18n.t('LABEL.MY_LINKS')}</span>
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="text-light linkContent pr-4 col-4 text-right">
            <div className="dropdown">
              <button
                className="btn bg-transparent"
                type="button"
                id="profileDropdown"
                data-toggle="dropdown"
              >
                <span className="profile">
                  {
                    <span className="userShortHand bg-light text-dark">
                      {this.props.firstName ? this.props.firstName[0] : ''}
                      {this.props.lastName ? this.props.lastName[0] : ''}
                    </span>
                  }
                </span>
              </button>
              <div className="dropdown-menu" aria-labelledby="profileDropdown">
                <button type="button" className="dropdown-item" onClick={this.handleLogout}>Logout</button>
              </div>
            </div>
          </div>

        </header>

      </React.Fragment>
    );
  }
}

Header.propTypes = {
  onlogout: PropTypes.func.isRequired,

  // getUser: PropTypes.func.isRequired,
  firstName: PropTypes.string,
  lastName: PropTypes.string
  // defaultChoice: PropTypes.string.isRequired

};

Header.defaultProps = {
  firstName: '',
  lastName: ''

};

const mapDispatchToProps = dispatch => ({
  getUser: userId => dispatch(UserAction.getUser(userId)),
  onlogout: () => dispatch(UserAction.onlogout())

});

const mapStateToProps = state => ({
  userId: state.user.get('userId'),
  firstName: state.user.get('firstName'),
  lastName: state.user.get('lastName'),
  role: state.user.get('role'),
  transferDetails: state.user.get('transferDetails'),
  receivedDetails: state.user.get('receivedDetails')

});

export default connect(mapStateToProps, mapDispatchToProps)(Header);

