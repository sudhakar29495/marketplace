import React from 'react';
import PropTypes from 'prop-types';

import '../../../assets/styles/components/AppHeader.scss';
import i18n from '../../i18next';

// const AppHeader = props => (
class AppHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isResponsiveOpen: false
    };
  }

  handleDeeptransfer = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  handleResponsive = () => {
    this.setState({ isResponsiveOpen: !this.state.isResponsiveOpen });
  }

  handleLogin =() => {
    this.setState({ isResponsiveOpen: !this.state.isResponsiveOpen });
    this.props.history.push('/login');
  }

  render() {
    const { isOpen, isResponsiveOpen } = this.state;
    return (
      <React.Fragment>
        <header className="deepHeader d-flex justify-content-between pl-2">
          <div>
            <img
              src="https://cdn.deeptransfer.net/assets/logo-crest.svg"
              className="logo"
              alt="logo"
            />
            <img
              src="https://cdn.deeptransfer.net/assets/logo-tekst.svg"
              className="logoText"
              alt="DEEPTRANSFER"
            />
          </div>
          <div className={`text-light linkContent pr-4 ${isOpen ? 'show' : ''}`}>
            <span
              role="presentation"
              className="mr-4 pointer"
            >
              <a href="http://blog.deeptransfer.net/" target="_blank" className="non-link">Blog</a>
            </span>
            <span
              role="presentation"
              className="mr-4 pointer"
              onClick={this.handleDeeptransfer}
            >{i18n.t('PLAN.WHY_DEEPTRANSFER')}
            </span>
            <span role="presentation" className="mr-4 pointer">
              <a href="mailto:enquiries@deepcloudai.com" className="non-link">{i18n.t('PLAN.SUPPORT')}</a>
            </span>
            <span role="presentation" className="pointer">
              <span
                // href="/login"
                role="presentation"
                onClick={() => this.props.history.push('/login')}
                className="non-link"
              >{i18n.t('LABEL.LOGIN')}
              </span>
            </span>
          </div>
          <div className="sandwichIcon">
            <i role="presentation" className="fas fa-bars text-light pointer" onClick={this.handleResponsive} />
          </div>
        </header>

        <div className={`why__deep--transfer z-1000 ${isOpen ? 'show' : ''}`}>
          <div className="why__deep--transfer-inner">
            <div className="close-line">
              <button className="close-button show" onClick={this.handleDeeptransfer}>
                <span className="icon" />
              </button>
            </div>
            <div className="why__deep--transfer-inner-content">

              <div className="top-line">
                <h2 className="content-title">{i18n.t('LABEL.DEEPTRANSFER')}</h2>
                <div className="socials__media--box">
                  <span className="socials__media--box-title">{i18n.t('LABEL.SOCIAL_MEDIA')}</span>
                  <ul className="socials-media-list">
                    <li>
                      <a
                        href="https://t.me/DeepCloudAI_ANN"
                        target="_blank"
                      ><i className="fab fa-telegram-plane" />
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://twitter.com/DeepCloud_AI"
                        target="_blank"
                      ><i className="fab fa-twitter" />
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://medium.com/@DeepCloud_AI"
                        target="_blank"
                      ><i className="fab fa-medium-m" />
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://www.youtube.com/channel/UCsFttSUlJuYbDvAiED_MlRg"
                        target="_blank"
                      ><i className="fab fa-youtube" />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <section className="scrollWrapper">
                <section className="wrap">
                  <div className="qa__item">
                    <h5 className="question">{i18n.t('PLAN.WHY_DEEPTRANSFER')}</h5>
                    <p>
                      {i18n.t('APP_HEADER.FAQ')}
                    </p>
                  </div>
                  <div className="qa__item">
                    <h5 className="question">
                      {i18n.t('APP_HEADER.FAQ1')}
                    </h5>
                    <p>
                      {i18n.t('APP_HEADER.FAQ1_ANSWER')}
                    </p>
                  </div>
                  <div className="qa__item">
                    <h5 className="question">
                      {i18n.t('APP_HEADER.FAQ2')}
                    </h5>
                    <p>
                      {i18n.t('APP_HEADER.FAQ2_ANSWER')}
                    </p>
                  </div>
                  <div className="qa__item">
                    <h5 className="question">
                      {i18n.t('APP_HEADER.FAQ3')}
                    </h5>
                    <p>
                      {i18n.t('APP_HEADER.FAQ3_ANSWER')}
                    </p>
                  </div>
                  <div className="qa__item">
                    <h5 className="question">{i18n.t('APP_HEADER.FAQ4')}</h5>
                    <p>{i18n.t('APP_HEADER.FAQ4_ANSWER')}
                    </p>
                  </div>

                  <div className="qa__item">
                    <h5 className="question">
                      {i18n.t('APP_HEADER.FAQ5')}
                    </h5>
                    <p>{i18n.t('APP_HEADER.FAQ5_ANSWER')}
                    </p>
                  </div>
                  <div className="qa__item">
                    <h5 className="question">{i18n.t('APP_HEADER.FAQ6')}</h5>
                    <p>{i18n.t('APP_HEADER.FAQ6_ANSWER')}
                    </p>
                  </div>
                  <div className="qa__item">
                    <h5 className="question">{i18n.t('APP_HEADER.FAQ7')}</h5>
                    <p>{i18n.t('APP_HEADER.FAQ7_ANSWER')}
                    </p>
                  </div>
                  <div className="qa__item">
                    <h5 className="question">{i18n.t('APP_HEADER.FAQ8')}</h5>
                    <p>{i18n.t('APP_HEADER.FAQ8_ANSWER')}
                    </p>
                  </div>
                  <div className="qa__item">
                    <h5 className="question">{i18n.t('APP_HEADER.FAQ9')}</h5>
                    <p>{i18n.t('APP_HEADER.FAQ9_ANSWER')}
                    </p>
                  </div>
                  <div className="qa__item">
                    <h5 className="question">{i18n.t('APP_HEADER.WHAT_OTHER_FEATURES')}
                    </h5>
                    <ul className="half-sides">
                      <li>{i18n.t('APP_HEADER.FEATURES1')}</li>
                      <li>{i18n.t('APP_HEADER.FEATURES2')}</li>
                      <li>{i18n.t('APP_HEADER.FEATURES3')}</li>
                      <li>{i18n.t('APP_HEADER.FEATURES4')}</li>
                      <li>{i18n.t('APP_HEADER.FEATURES5')}</li>
                      <li>{i18n.t('APP_HEADER.FEATURES6')}</li>
                      <li>{i18n.t('APP_HEADER.FEATURES7')}</li>
                      <li>{i18n.t('APP_HEADER.FEATURES8')}</li>
                    </ul>
                  </div>
                </section>
              </section>

            </div>
          </div>
        </div>
        <div className={`why__deep--transfer responsiveContainer ${isResponsiveOpen ? 'show' : ''}`}>
          <div className="why__deep--transfer-inner d-flex justify-content-center align-items-center">

            <div className="d-flex flex-column sandwichFontContainer ">
              <span className="sandwichFont">
                <a href="http://blog.deeptransfer.net/" target="_blank" className="non-link">Blog</a>
              </span>
              <span
                className="sandwichFont"
                role="presentation"
                onClick={this.handleDeeptransfer}
              >{i18n.t('PLAN.WHY_DEEPTRANSFER')}
              </span>
              <span className="sandwichFont">
                <a href="mailto:enquiries@deepcloudai.com" className="non-link">{i18n.t('PLAN.SUPPORT')}</a>
              </span>
              <span role="presentation" className="sandwichFont">
                <span
                  role="presentation"
                  onClick={() => this.handleLogin()}
                  className="non-link"
                >{i18n.t('LABEL.LOGIN')}
                </span>
              </span>
              <div className="socialEvents">
                <ul>
                  <li>
                    <a
                      href="https://t.me/DeepCloudAI_ANN"
                      target="_blank"
                    ><i className="fab fa-telegram-plane" />
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://twitter.com/DeepCloud_AI"
                      target="_blank"
                    ><i className="fab fa-twitter" />
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://medium.com/@DeepCloud_AI"
                      target="_blank"
                    ><i className="fab fa-medium-m" />
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://www.youtube.com/channel/UCsFttSUlJuYbDvAiED_MlRg"
                      target="_blank"
                    ><i className="fab fa-youtube" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>

          </div>
        </div>
      </React.Fragment>
    );
  }
}

AppHeader.propTypes = {
  // handleLogin: PropTypes.func,
  history: PropTypes.object.isRequired
};

AppHeader.defaultProps = {
  // handleLogin: () => {}

};

export default AppHeader;
