import React from 'react';
import { ValidatorComponent } from 'react-form-validator-core';

class TextGroupValidator extends ValidatorComponent {
  render() {
    const {
      errorMessages, validators, requiredError, append, validatorListener, ...rest
    } = this.props;

    return (
      <div>
        <div className="input-group">
          <input
            autoComplete="off"
            autoCorrect="off"
            autoCapitalize="none"
            spellCheck="false"
            {...rest}
            ref={r => { this.input = r; }}
          />
          <div className="input-group-append">
            <span className="input-group-text">{append}</span>
          </div>
        </div>
        {this.errorText()}
      </div>
    );
  }

  errorText() {
    const { isValid } = this.state;

    if (isValid) {
      return null;
    }

    return (
      <div style={{ color: 'red' }}>
        {this.getErrorMessage()}
      </div>
    );
  }
}

export default TextGroupValidator;
