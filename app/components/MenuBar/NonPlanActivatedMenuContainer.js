import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import i18n from '../../i18next';

const NonPlanActivatedMenuContainer = props => (
  <ul className="menu-container">
    <div className="text-center my-3">
      <li className="menu-items" onClick={props.closeResponsive} role="presentation">
        <NavLink
          exact
          to="/"
          className="navLink dashboard"
          activeclassname="active"
        >
          <span>{i18n.t('LABEL.ACTIVATE_PLAN')}</span>
        </NavLink>
      </li>
    </div>
    <li className="menu-items" onClick={props.closeResponsive} role="presentation">
      <NavLink
        exact
        to="/profile"
        className="navLink dashboard"
        activeclassname="active"
      >
        <i className="navIcon profile" />
        <span>{i18n.t('LABEL.PROFILE')}</span>
      </NavLink>
    </li>
  </ul>
);

NonPlanActivatedMenuContainer.propTypes = {
  closeResponsive: PropTypes.func
};

NonPlanActivatedMenuContainer.defaultProps = {
  closeResponsive: () => {}
};

export default NonPlanActivatedMenuContainer;
