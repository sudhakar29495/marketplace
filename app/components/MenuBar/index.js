import React from 'react';
import PropTypes from 'prop-types';
import '../../../assets/styles/components/MenuBar.scss';
import MenuContainer from './MenuContainer';
// import store from '../../globals/localStorage';
// import UserAction from '../../actions/user.js';
import NonPlanActivatedMenuContainer from './NonPlanActivatedMenuContainer';
import i18n from '../../i18next';

const MenuBar = props => (
  <aside className="hr-menubar">
    <div className="app-logo">
      <span>
        <img
          src="https://cdn.deeptransfer.net/assets/logo-crest.svg"
          className="logo"
          alt="logo"
        />
        <img
          src="https://cdn.deeptransfer.net/assets/logo-tekst.svg"
          className="logoText"
          alt="DEEPTRANSFER"
        />
        <span className="premiumSubTitle">{i18n.t('LABEL.PREMIUM')}</span>
      </span>
    </div>
    {props.isPlanActivated &&
      <MenuContainer
        {...props}
      />
    }
    {!props.isPlanActivated &&
      <NonPlanActivatedMenuContainer {...props} />
    }
  </aside>
);
MenuBar.propTypes = {
  history: PropTypes.object.isRequired,
  plan: PropTypes.object.isRequired,
  isPlanActivated: PropTypes.bool
};

MenuBar.defaultProps = {
  isPlanActivated: false
};

export default MenuBar;
