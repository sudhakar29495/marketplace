import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import i18n from '../../i18next';

const MenuContainer = props => (
  <Fragment>
    <ul className="menu-container">
      <div className="text-center my-3">
        <button
          className="btn btn-success"
          onClick={() => { props.history.push('/'); props.closeResponsive(); }}

        >{i18n.t('TRANSFERS.TRANSFER_NEW_FILES')}
        </button>
      </div>
      <li className="menu-items" onClick={props.closeResponsive} role="presentation">
        <NavLink
          exact
          to="/fileManager"
          className="navLink dashboard"
          activeclassname="active"
        >
          <i className="navIcon transfer" />
          <span>{i18n.t('LABEL.FILE_MANAGER')}</span>
        </NavLink>
      </li>
      <li className="menu-items" onClick={props.closeResponsive} role="presentation">
        <NavLink
          exact
          to="/profile"
          className="navLink dashboard"
          activeclassname="active"
        >
          <i className="navIcon profile" />
          <span>{i18n.t('LABEL.PROFILE')}</span>
        </NavLink>
      </li>
    </ul>
    <div className="textColor">
      <div className="d-flex">
        <i className="fal fa-cloud-upload   mx-3 mt-1" />
        <span className="">{i18n.t('LABEL.STORAGE')}</span>
      </div>
      <div className="m-3">
        <div className="progress h-1">
          <div
            className="progress-bar "
            style={{ width: `${(props.completedPercentage).toFixed(2)}%` }}
            role="progressbar"
            aria-valuenow="25"
            aria-valuemin="0"
            aria-valuemax="100"
          />
        </div>
      </div>
      <span className="m-3 font-14">
        <span>{i18n.t('TRANSFERS.STORAGE_USED', { consumed: props.consumedLimit, limit: props.maxStoreLimit })}</span>
      </span>
    </div>
  </Fragment>
);

MenuContainer.propTypes = {
  history: PropTypes.object.isRequired,
  closeResponsive: PropTypes.func,
  completedPercentage: PropTypes.number,
  maxStoreLimit: PropTypes.string,
  consumedLimit: PropTypes.string
};

MenuContainer.defaultProps = {
  closeResponsive: () => {},
  completedPercentage: 0,
  maxStoreLimit: '',
  consumedLimit: ''
};

export default MenuContainer;
