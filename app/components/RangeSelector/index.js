import React from 'react';
import PropTypes from 'prop-types';
import '../../../assets/styles/components/RangeSelector.scss';

class RangeSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timeInterval: this.props.timeInterval,
      disabledButtons: this.props.disabledButtons
    };
  }

  render() {
    const { timeInterval, disabledButtons } = this.state;
    const { selectTimeInterval, activeTimePeriod, className } = this.props;
    return (
      <div className={className !== '' ? `selector btn-group ${className}` : 'selector btn-group'} role="group">
        {timeInterval && timeInterval.map(data => (
          <button
            key={data}
            type="button"
            disabled={!(disabledButtons.indexOf(data) === -1)}

            onClick={() => selectTimeInterval({ data })}
            className={(activeTimePeriod === data ?
              'btn btn-outline-secondary activeTimePeriodBtn py-0' :
              'btn btn-outline-secondary  py-0')}
          >
            {data}

          </button>

        ))
        }
      </div>
    );
  }
}

RangeSelector.propTypes = {
  timeInterval: PropTypes.array,
  activeTimePeriod: PropTypes.string,
  className: PropTypes.string,
  disabledButtons: PropTypes.array,
  selectTimeInterval: PropTypes.func.isRequired
};

RangeSelector.defaultProps = {
  timeInterval: [],
  disabledButtons: [],
  activeTimePeriod: '',
  className: ''
};

export default RangeSelector;
