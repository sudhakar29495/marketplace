import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const CustomDatePicker = props => (
  <DatePicker
    {...props}
  />
);

export default CustomDatePicker;
