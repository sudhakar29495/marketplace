import { fromJS } from 'immutable';

import userActions from '../constants/userAction';

const initialState = fromJS({
  loginError: '',
  isLoginSuccess: '',
  userId: '',
  accessToken: '',
  firstName: '',
  lastName: '',
  email: '',
  role: '',
  userError: '',
  balance: '',
  balanceLoading: false,
  createUserLoader: false,
  planDetails: [],
  plan: {}
});

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case userActions.UPDATE_USER_ID:
      return state.set('userId', action.userId);

    case userActions.GET_AUDIT_SUCCESS:
      return state
        .set('auditDetails', action.response)
        .set('auditDetailsLoader', false);

    case userActions.GET_AUDIT:
      return state.set('auditDetailsLoader', true);

    case userActions.GET_AUDIT_ERROR:
      return state.set('auditDetailsLoader', false);

    case userActions.USER_LOGIN:
      return state
        .set('loading', true)
        .set('loginError', '')
        .set('emailVerified', null);

    case userActions.USER_LOGIN_SUCCESS:
      return state
        .set('userId', action.user.userId)
        .set('accessToken', action.accessToken && action.accessToken.id)
        .set('firstName', action.user.first_name)
        .set('lastName', action.user.last_name)
        .set('email', action.user.email)
        .set('emailVerified', action.user.emailVerified)
        .set('plan', action.user.plan)

        .set('role', action.user.roles.length ? action.user.roles[0].name : 'ADMIN');
    case userActions.RESET_PASSWORD:
      return state
        .set('ForgetPasswordError', '');
    case userActions.GET_PLAN_SUCCESS:
      return state
        .set('planDetails', action.response);

    case userActions.RESET_PASSWORD_ERROR:
      return state
        .set('ForgetPasswordError', action.error.message);
    case userActions.RESET_PASSWORD_SUCCESS:
      return state
        .set('resetPasswordResponse', action.response);
    case userActions.CREATE_USER:
      return state.set('createUserLoader', true);
    case userActions.CREATE_USER_ERROR:
      return state.set('createUserLoader', false);

    case userActions.GET_CONSUMED_STORAGE_SUCCESS:
      return state.set('consumedLimit', action.response);

    case userActions.CREATE_USER_SUCCESS:
      return state

        .set('userId', action.user.id)
        .set('createUserLoader', false)
        // .set('accessToken', action.accessToken.id)
        .set('firstName', action.user.first_name)
        .set('lastName', action.user.last_name)
        .set('email', action.user.email)
        .set('role', action.user.roles.length ? action.user.roles[0].name : 'ADMIN');
    case userActions.USER_LOGIN_ERROR:
      return state

        .set('loginError', action.error.message);

    case userActions.GET_BALANCE:
      return state
        .set('balanceLoading', true);
    case userActions.GET_BALANCE_SUCCESS:
      return state
        .set('balanceLoading', false)
        .set('balance', action.response);

    case userActions.GET_TRANSFER_DETAILS_SUCCESS:
      return state
        .set('transferDetails', action.response)
        .set('transferLoader', false);

    case userActions.GET_TRANSFER_DETAILS_ERROR:
      return state
        .set('transferLoader', false);

    case userActions.GET_TRANSFER_DETAILS:
      return state
        .set('transferLoader', true);

    case userActions.GET_RECEIVED_DETAILS_SUCCESS:
      return state
        .set('receivedDetails', action.response)
        .set('receivedDetailsLoader', true);

    case userActions.GET_RECEIVED_DETAILS_ERROR:
      return state
        .set('receivedDetailsLoader', false);

    case userActions.GET_RECEIVED_DETAILS:
      return state
        .set('receivedDetailsLoader', false);

    case userActions.GET_FILE_SIZE:
      return state
        .set('fileSize', action.response)
        .set('fileSizeLoader', true);

    case userActions.GET_FILE_SIZE_ERROR:
      return state
        .set('fileSizeLoader', false);

    case userActions.GET_FILE_SIZE_SUCCESS:
      return state
        .set('fileSizeLoader', false);
    case userActions.CLEAR_ERROR:
      return state
        .set('loginError', '')
        .set('ForgetPasswordError', '')
        .set('emailVerified', null)

        .set('resetPasswordError', '');

    case userActions.RESET_TOKEN_PASSWORD:
      return state
        .set('resetPasswordError', '');
    case userActions.RESET_TOKEN_PASSWORD_ERROR:
      return state
        .set('resetPasswordError', action.error.message);
    // case userActions.RESET_TOKEN_PASSWORD_SUCCESS:
    //   return state
    //     .set('fileSizeLoader', false);

    case userActions.GET_BALANCE_ERROR:
      return state
        .set('balanceLoading', false);
    case userActions.GET_USER_SUCCESS:
      return state
        .set('userId', action.user.id)
        .set('firstName', action.user.first_name)
        .set('lastName', action.user.last_name)
        .set('plan', action.user.plan)
        .set('role', action.user.roles.length ? action.user.roles[0].name : 'ADMIN')
        .set('email', action.user.email);
    case userActions.GET_USER_ERROR:
      return state
        .set('userError', action.error);
    case userActions.GET_PASSPHRASE_SUCCESS:
      return state
        .set('passPhrase', action.response);
    case userActions.CHANGE_PASSWORD_SUCCESS:
      return state
        .set('changePasswordResponse', action.response);
    case userActions.UPDATE_USER_DETAIL_SUCCESS:
      return state
        .set('firstName', action.response.first_name)
        .set('lastName', action.response.last_name);
    case userActions.VERIFY_USER_SUCCESS:
      return state
        .set('verifyResponse', action.response);

    case userActions.LOGOUT_USER_SUCCESS:
      return initialState;

    default:
      return state;
  }
};

export default userReducer;
