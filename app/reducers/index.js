import { combineReducers } from 'redux';
import userReducer from './userReducer';

const appReducer = combineReducers({
  user: userReducer
});

const rootReducer = (state, action) => {
  if (action.type === 'USERS/LOGOUT_USER') {
    state = {};
  }

  return appReducer(state, action);
};

export default rootReducer;
