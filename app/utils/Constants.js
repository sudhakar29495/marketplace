const NETWORKS = {
  0: 'Main Net',
  1: 'Test Net',
  2: 'Private Chain'
};

module.exports = {
  NETWORKS
};
