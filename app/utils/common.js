const dateFormater = date => {
  let formatedDate = new Date(date).toUTCString();
  formatedDate = formatedDate.split(' ').slice(1, 4).join(' ');
  return formatedDate;
};

module.exports = {
  dateFormater
};
