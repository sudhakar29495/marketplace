import axios from 'axios/index';

export default class UserService {
  static loginUser(email, password) {
    return axios({
      method: 'post',
      url: '/api/Users/login',
      data: {
        email,
        password,
        type: 'REQUESTOR'
      }
    });
  }

  static logout() {
    return axios({
      method: 'get',
      url: '/logout'
    });
  }

  static getUser(userId) {
    return axios({
      method: 'get',
      url: `/api/Users/${userId}`
    });
  }
}
