import BaseAction from './BaseAction';
import constants from '../constants/userAction';

class UserAction extends BaseAction {
  constructor() {
    super({
      onlogin: {
        type: constants.USER_LOGIN,
        params: ['email', 'password', 'cb']
      },
      onLoginUserSuccess: {
        type: constants.USER_LOGIN_SUCCESS,
        params: ['user', 'accessToken']
      },
      onLoginUserError: {
        type: constants.USER_LOGIN_ERROR,
        params: ['error']
      },
      onlogout: {
        type: constants.LOGOUT_USER,
        params: ['cb']
      },
      onlogoutSuccess: {
        type: constants.LOGOUT_USER_SUCCESS,
        params: []
      },
      onlogoutError: {
        type: constants.LOGOUT_USER_ERROR,
        params: ['error']
      },
      getUser: {
        type: constants.GET_USER,
        params: ['userId']
      },
      getUserSuccess: {
        type: constants.GET_USER_SUCCESS,
        params: ['user']
      },
      getUserError: {
        type: constants.GET_USER_ERROR,
        params: ['error']
      }
    });
  }
}

export default new UserAction();
