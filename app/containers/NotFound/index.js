import React from 'react';

const NotFound = () =>
  (
    <div className="container-fluid">
      <div className="row">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div className="notFound">
            <section className="text-center content">
              <p>Page not found</p>
            </section>
          </div>
        </div>
      </div>
    </div>
  );

export default NotFound;
