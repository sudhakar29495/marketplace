import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

import SideMenu from '../SideMenu';

/**
 * * Top level redirection to login page
 * Note : Do not use render with custom props in top level,
 * Specify the extra props if any in 'additionalProps' prop as an object
 * @param Component
 * @param rest
 * @returns {*}
 * @constructor
 */
const RedirectRoute = ({
  component: Component, userId, path, ...rest
}) => (
  <Route
    {...rest}
    render={props => (
      <Fragment>
        {/* eslint-disable no-unused-expressions */}
        {do {
          if (userId === '') {
            <Fragment>
              <section className="body-section">
                <SideMenu {...props} />
                <div className="container-section">
                  <Component {...props} />
                </div>
              </section>
            </Fragment>;
          } else {
            <Fragment />;
          }
        }
      }
      </Fragment>
    )}
  />
);

RedirectRoute.propTypes = {
  component: PropTypes.func.isRequired,
  userId: PropTypes.string,
  path: PropTypes.string.isRequired
};

RedirectRoute.defaultProps = {
  userId: ''
};

const mapStateToProps = state => ({
  userId: state.user.get('userId')
});

export default connect(mapStateToProps)(RedirectRoute);
