import React from 'react';
import { Switch, Route } from 'react-router-dom';

import RedirectRoute from './RedirectRoute';
import NotFound from '../NotFound';
import HomePage from '../HomePage';

const MainRoutes = () => (
  <Switch>
    <RedirectRoute path="/" exact component={HomePage} />

    <Route path="*" component={NotFound} />
  </Switch>
);

export default MainRoutes;
