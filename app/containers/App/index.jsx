import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import MainRoutes from '../Routes';
import '../../../assets/styles/index.scss';
import '../../../assets/styles/containers/App.scss';

const App = () => (
  <Fragment>
    <ToastContainer
      position="top-right"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnVisibilityChange
      draggable
      pauseOnHover
    />
    <MainRoutes />
  </Fragment>
);

export default withRouter(connect(null, null)(App));
