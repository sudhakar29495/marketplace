import { takeLatest } from 'redux-saga/effects';
import userActions from '../constants/userAction';
import { loginUser, logoutUser, getUser } from './userSaga';

export default function* saga() {
  yield takeLatest(userActions.USER_LOGIN, loginUser);
  yield takeLatest(userActions.LOGOUT_USER, logoutUser);
  yield takeLatest(userActions.GET_USER, getUser);
}
