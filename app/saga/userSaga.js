import { call, put } from 'redux-saga/effects';

import storage from '../globals/localStorage';
import UserActions from '../actions/user';
import UserService from '../service/user';
import { setAuthToken } from '../globals/interceptors';

/**
 *Generator method for login
 * @param {*} action
 */
function* loginUser(action) {
  try {
    const { email, password, cb } = action;
    const response = yield call(UserService.loginUser, email, password);
    storage.setItem('user', response.user.id);
    if (response.accessToken) {
      storage.setItem('accessToken', response.accessToken.id);
    }
    yield put(UserActions.onLoginUserSuccess(response.user, response.accessToken));
    yield call(setAuthToken);
    cb();
  } catch (e) {
    yield put(UserActions.onLoginUserError(e));
  }
}

/**
 * Generator method for logout
 */
function* logoutUser(action) {
  try {
    const { cb } = action;
    yield call(UserService.logout);
    if (cb) {
      cb();
    }
    yield put(UserActions.onlogoutSuccess());
  } catch (e) {
    yield put(UserActions.onlogoutError(e));
  }
}

/**
 * Generator method to fetch user
 */
function* getUser(action) {
  try {
    const { userId } = action;
    const response = yield call(UserService.getUser, userId);
    yield put(UserActions.getUserSuccess(response));
  } catch (e) {
    yield put(UserActions.getUserError(e));
  }
}

module.exports = {
  loginUser,
  logoutUser,
  getUser
};
