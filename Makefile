build:
	docker build -t huron/marketplace-ui:latest .

build-docker-image: build
	docker tag huron/marketplace-ui:latest quay.io/huron/marketplace-ui:${BITBUCKET_COMMIT}
	docker login quay.io -u ${QUAY_USER} -p ${QUAY_PWD}
	docker push quay.io/huron/marketplace-ui:${BITBUCKET_COMMIT}
